package yanlin.hadoop.mr;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * @author yanlin
 * @date 2018/7/17 下午3:27
 */
public class WordCountPartitioner {

    // 继承Mapper类,Mapper类的四个泛型参数分别为：输入key类型，输入value类型，输出key类型，输出value类型
    public static class MyMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

        private final static IntWritable one = new IntWritable(1); // output value
        private Text word = new Text(); // output key

        @Override
        public void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {

            String line = value.toString();
            String[] splited = line.split(" ");
            //foreach 就是 for（元素类型t 元素变量x:遍历对象obj）{引用x的java语句}
            for (String word : splited) {
                context.write(new Text(word), new IntWritable(1));
            }

        }
    }



    public static class ProviderPartitioner extends Partitioner<Text, IntWritable> {


        @Override
        public int getPartition(Text text, IntWritable intWritable, int i) {

            String str = text.toString();
            switch (str){
                case "hello":return 1;
                case"hei":return 2;
                default:return 0;
            }
        }
    }


        // Reduce类，继承了Reducer类
    public static class MyReducer extends org.apache.hadoop.mapreduce.Reducer<Text, IntWritable, Text, IntWritable>{
        @Override
        protected void reduce(Text k2, Iterable<IntWritable> v2s,
                              Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {

            int count = 0;
            for (IntWritable v2 : v2s) {
                count += v2.get();
            }
            IntWritable v3 = new IntWritable(count);
            context.write(k2, v3);
        }
    }

    public static void main(String[] args) throws Exception {



        Job job = new Job(); // 创建一个作业对象
        job.setJarByClass(WordCountPartitioner.class); // 设置运行/处理该作业的类
        job.setJobName("WordCount");

        job.setMapperClass(MyMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path("hdfs://10.4.231.175:9000/bigerword.txt"));

        job.setReducerClass(MyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileOutputFormat.setOutputPath(job, new Path("/Users/yanlin/ideaprojects/datahadoop"));

        job.setPartitionerClass(ProviderPartitioner.class);
        job.setNumReduceTasks(3);

        System.exit(job.waitForCompletion(true) ? 0 : 1);

    }
}
